package vadebici.dom;

public enum BicicletaStatus {
    DISPONIVEL, APOSENTADA, NOVA, OCUPADA, MANUTENCAO
}
