package vadebici.dom;

public enum TrancaStatus {
    DISPONIVEL, OCUPADA, MANUTENCAO, APOSENTADA, NOVA
}
