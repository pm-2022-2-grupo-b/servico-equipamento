package vadebici.util;

import io.javalin.plugin.json.JavalinJson;
import vadebici.controller.TotemController;
import vadebici.controller.TrancaController;
import vadebici.controller.BicicletaController;
import vadebici.controller.EmailController;
import vadebici.exception.GenericApiException;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
    private static String PATH_TRANCA = "/tranca/:idTranca";
    private static String PATH_BICICLETA = "/bicicleta";
    private static String PATH_BICICLETA_ID = "/bicicleta/:idBicicleta";

    private final Javalin app =
            Javalin.create(config -> config.defaultContentType = "application/json")
                    .routes(() -> {
                        path("/enviarEmail", () -> post(EmailController::enviarEmail));

                        path("/totem", () -> get(TotemController::buscarTotens));
                        path("/totem", () -> post(TotemController::salvarTotem));
                        path("/totem/:idTotem", () -> delete(TotemController::deletarTotem));
                        path("/totem/:idTotem", () -> put(TotemController::atualizarTotem));
                        path("/totem/:idTotem/trancas", () -> get(TotemController::buscarTrancasPorTotem));
                        path("/totem/:idTotem/bicicletas", () -> get(TotemController::buscarBicicletasPorTotem));
                        
                        path("/tranca", () -> get(TrancaController::buscarTrancas));
                        path("/tranca", () -> post(TrancaController::salvarTranca));
                        path(PATH_TRANCA, () -> get(TrancaController::buscarTranca));
                        path(PATH_TRANCA, () -> delete(TrancaController::deletarTranca));
                        path(PATH_TRANCA, () -> put(TrancaController::atualizarTranca));
                        path(PATH_TRANCA + "/status/:acao", () -> post(TrancaController::alterarStatus));
                        path("/tranca/integrarNaRede", () -> post(TrancaController::integrarNaRede));
                        path("/tranca/removerDaRede", () -> post(TrancaController::removerDaRede));
                        path(PATH_TRANCA + PATH_BICICLETA, () -> get(TrancaController::buscaBicicletaPorTranca));
                        
                        path(PATH_BICICLETA, () -> get(BicicletaController::buscarBicicletas));
                        path(PATH_BICICLETA, () -> post(BicicletaController::salvarBicicleta));
                        path(PATH_BICICLETA_ID, () -> get(BicicletaController::buscarBicicleta));
                        path(PATH_BICICLETA_ID, () -> delete(BicicletaController::deletarBicicleta));
                        path(PATH_BICICLETA_ID, () -> put(BicicletaController::atualizarBicicleta));
                        path(PATH_BICICLETA_ID + "/status/:acao", () -> post(BicicletaController::alterarStatus));
                        path("/bicicleta/integrarNaRede", () -> post(BicicletaController::integrarNaRede));
                        path("/bicicleta/removerDaRede", () -> post(BicicletaController::removerDaRede));
                    })
                    .error(500, ctx -> {
                        GenericApiException ex = new GenericApiException(500, "Ocorreu um erro durante a operação");
                        ctx.result(JavalinJson.toJson(ex));
                    }).error(404, ctx -> {
                        if (ctx.attribute("handle") == null) {
                            GenericApiException ex = new GenericApiException(404, "Página não encontrada");
                            ctx.result(JavalinJson.toJson(ex));
                        }
                    })
                    .exception(GenericApiException.class, (e, ctx) -> {
                        ctx.attribute("handle", true);
                        String response = JavalinJson.getToJsonMapper().map(e);
                        ctx.status(e.getCodigo());
                        ctx.result(response);
                    });



    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
