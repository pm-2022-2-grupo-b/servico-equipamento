## Sonar
[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-equipamento)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-equipamento&metric=bugs)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-equipamento)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-equipamento&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-equipamento)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-equipamento&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-equipamento)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-equipamento&metric=coverage)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-equipamento)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=pm-2022-2-grupo-b_servico-equipamento&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=pm-2022-2-grupo-b_servico-equipamento)